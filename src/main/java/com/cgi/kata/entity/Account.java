package com.cgi.kata.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Account {
	
	private long id;
	private BigDecimal balance;
	private Client client;
	private List<Operation> operations;
	/**
	 * @param id
	 * @param balance
	 * @param client
	 */
	public Account(long id, BigDecimal balance, Client client) {
		super();
		this.id = id;
		this.balance = balance;
		this.client = client;
		operations = new ArrayList<Operation>();
	}
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the balance
	 */
	public BigDecimal getBalance() {
		return balance;
	}
	/**
	 * @param balance the balance to set
	 */
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	/**
	 * @return the client
	 */
	public Client getClient() {
		return client;
	}
	/**
	 * @param client the client to set
	 */
	public void setClient(Client client) {
		this.client = client;
	}
	/**
	 * @return the operations
	 */
	public List<Operation> getOperations() {
		return operations;
	}
	

}
