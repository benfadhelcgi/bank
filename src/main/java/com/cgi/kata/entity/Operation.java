package com.cgi.kata.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.cgi.kata.util.OperationEnum;

public class Operation {
	private OperationEnum operationType;
	private LocalDateTime date;
	private BigDecimal amount;
	private BigDecimal balance;
	/**
	 * @param operationType
	 * @param date
	 * @param balance
	 */
	public Operation(OperationEnum operationType, LocalDateTime date, BigDecimal amount, BigDecimal balance) {
		super();
		this.operationType = operationType;
		this.date = date;
		this.amount = amount;
		this.balance = balance;
	}
	/**
	 * @return the operationType
	 */
	public OperationEnum getOperationType() {
		return operationType;
	}
	/**
	 * @param operationType the operationType to set
	 */
	public void setOperationType(OperationEnum operationType) {
		this.operationType = operationType;
	}
	/**
	 * @return the date
	 */
	public LocalDateTime getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(LocalDateTime date) {
		this.date = date;
	}
	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	/**
	 * @return the balance
	 */
	public BigDecimal getBalance() {
		return balance;
	}
	/**
	 * @param balance the balance to set
	 */
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
}
