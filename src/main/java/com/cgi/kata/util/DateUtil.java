package com.cgi.kata.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateUtil {
	
	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

	public static String fromLocalDateTimeToString(LocalDateTime localDateTime) {
		String formattedDateTime = localDateTime.format(formatter);
		return formattedDateTime;
	}


}
