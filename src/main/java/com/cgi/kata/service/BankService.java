package com.cgi.kata.service;

import java.math.BigDecimal;
import java.util.List;

import com.cgi.kata.entity.Account;
import com.cgi.kata.exception.InsufficientBalanceException;

public interface BankService {
	
	/**
	 * US1 : make a deposit in my account
	 * @param account
	 * @param amount
	 */
	public void saveMoney(Account account, BigDecimal amount);
	
	/**
	 * US2 : make a withdrawal from my account
	 * @param account
	 * @param amount
	 * @throws InsufficientBalanceException
	 */
	public void retrieveMoney(Account account, BigDecimal amount) throws InsufficientBalanceException;
	
	/**
	 * US3 : see the history <b>(operation, date, amount, balance)</b> of my operations
	 * @param account
	 * @return
	 */
	public List<String> getOperationHistory(Account account);

}
