package com.cgi.kata.service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import com.cgi.kata.entity.Account;
import com.cgi.kata.entity.Operation;
import com.cgi.kata.exception.InsufficientBalanceException;
import com.cgi.kata.util.DateUtil;
import com.cgi.kata.util.OperationEnum;

public class BankServiceImpl implements BankService{

	public void saveMoney(Account account, BigDecimal amount) {
		Operation op = new Operation(OperationEnum.DEPOSIT, LocalDateTime.now(), amount, account.getBalance());
		BigDecimal newBalance = account.getBalance().add(amount);
		account.setBalance(newBalance);
		account.getOperations().add(op);
	}

	public void retrieveMoney(Account account, BigDecimal amount) throws InsufficientBalanceException {
		Operation op = new Operation(OperationEnum.WITHDRAWAL, LocalDateTime.now(), amount, account.getBalance());
		BigDecimal newBalance = account.getBalance().subtract(amount);
		if (newBalance.compareTo(new BigDecimal("0")) < 0) {
			throw new InsufficientBalanceException("insufficient balance in the account to do retreive!");
		} else {
			account.setBalance(newBalance);
			account.getOperations().add(op);
		}
	}

	public List<String> getOperationHistory(Account account) {
		List<String> operationHistory = account.getOperations().stream()
				.map(operation -> "OPERATION:" + operation.getOperationType().name() + ", DATE:"
						+ DateUtil.fromLocalDateTimeToString(operation.getDate()) + ", AMOUNT:" + operation.getAmount()
						+ ", BALANCE:" + operation.getAmount())
				.collect(Collectors.toList());
		return operationHistory;
	}

}
