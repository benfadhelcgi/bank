package com.cgi.kata.main;

import java.math.BigDecimal;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.cgi.kata.entity.Account;
import com.cgi.kata.entity.Client;
import com.cgi.kata.exception.InsufficientBalanceException;
import com.cgi.kata.service.BankService;
import com.cgi.kata.service.BankServiceImpl;

public class MainBank {
	private static final Logger LOG = LogManager.getLogger(MainBank.class);
	private void doBankOperations(Account account, BankService bankService) throws InsufficientBalanceException {
		bankService.saveMoney(account, new BigDecimal("10.0"));
		bankService.saveMoney(account, new BigDecimal("10.0"));
		bankService.saveMoney(account, new BigDecimal("10.0"));
		bankService.saveMoney(account, new BigDecimal("10.0"));
		bankService.retrieveMoney(account, new BigDecimal("10.0"));
		bankService.retrieveMoney(account, new BigDecimal("10.0"));
		bankService.retrieveMoney(account, new BigDecimal("10.0"));
	}
	public static void main(String[] args) {
		MainBank bank = new MainBank();
		Client c1 = new Client(1, "client1");
		Account ac1 = new Account(1, new BigDecimal("1000.0"), c1);
		BankService bankService = new BankServiceImpl();
		try {
			bank.doBankOperations(ac1, bankService);
		} catch (InsufficientBalanceException e) {
			LOG.error(e.getMessage());
		}
		List<String> accountHistory = bankService.getOperationHistory(ac1);
		accountHistory.forEach(System.out::println);
	}

}
