package com.cgi.kata.service;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import com.cgi.kata.entity.Account;
import com.cgi.kata.entity.Client;
import com.cgi.kata.exception.InsufficientBalanceException;
import com.cgi.kata.util.DateUtil;
import com.cgi.kata.util.OperationEnum;

public class BankServiceTest {

	BankService bankService;

	@Before
	public void init() {
		bankService = new BankServiceImpl();
	}

	@Test
	public void saveMoneyTest() {
		Client c1 = new Client(1, "client1");
		Account ac1 = new Account(1, new BigDecimal("1000.0"), c1);
		bankService.saveMoney(ac1, new BigDecimal("10.0"));
		assertEquals(ac1.getBalance(), new BigDecimal("1010.0"));
		assertEquals(ac1.getOperations().get(0).getOperationType(), OperationEnum.DEPOSIT);
	}

	@Test
	public void retrieveMoneyTest() throws InsufficientBalanceException {
		Client c1 = new Client(1, "client1");
		Account ac1 = new Account(1, new BigDecimal("1000.0"), c1);
		bankService.retrieveMoney(ac1, new BigDecimal("10.0"));
		assertEquals(ac1.getBalance(), new BigDecimal("990.0"));
		assertEquals(ac1.getOperations().get(0).getOperationType(), OperationEnum.WITHDRAWAL);

	}

	@Test(expected = InsufficientBalanceException.class)
	public void retrieveMoneyExceptionTest() throws InsufficientBalanceException {
		Client c1 = new Client(1, "client1");
		Account ac1 = new Account(1, new BigDecimal("1000.0"), c1);
		bankService.retrieveMoney(ac1, new BigDecimal("1010.0"));
	}

	@Test
	public void getOperationHistoryTest() throws InsufficientBalanceException {
		Client c1 = new Client(1, "client1");
		Account ac1 = new Account(1, new BigDecimal("1000.0"), c1);
		bankService.retrieveMoney(ac1, new BigDecimal("10.0"));
		List<String> operationHistoryExpected = ac1.getOperations().stream()
				.map(operation -> "OPERATION:" + operation.getOperationType().name() + ", DATE:"
						+ DateUtil.fromLocalDateTimeToString(operation.getDate()) + ", AMOUNT:" + operation.getAmount()
						+ ", BALANCE:" + operation.getAmount())
				.collect(Collectors.toList());
		List<String> operationHistory = bankService.getOperationHistory(ac1);
		assertEquals(operationHistoryExpected.get(0), operationHistory.get(0));
	}

}
